# AWS Certified Developer Study Guide - Chapter 5 Exercise

This folder contains AWS CDK and Python code to create the infrastructure for the
exercises of Chapter 5. This code has the advantage that the infrastructure can be
destroyed with one command by using the AWS CDK (CloudFormation) instead of manual
cleanup.

## Setup

[Python version 3.8 or newer](https://www.python.org/downloads/) and
[Node.js 11 or newer](https://nodejs.org/en/) must be installed for all exercises.

Install the CDK (all exercises):
```
npm install
```

Create a Python virtual environment and install all requirements (all exercises):
```
python -m venv venv
. venv/bin/activate
python -m pip install -r requirements.txt
```

## Exercise 5.1 - Deployment of the S3 Bucket with IAM Policies and upload of encrypted and unencrypted objects

Execute the following commands to deploy the S3 Bucket:
```
npx cdk bootstrap
npx cdk deploy S3Stack
```

Execute the following Python script to upload an encrypted object and try to upload an
unencrypted object:
```
python s3_upload.py
```

An `AccessDenied` error will occur where the script tries to put an unencrypted object
into the S3 Bucket.

Ensure that the encrypted object was successfully uploaded in the AWS Management
Console.

## Exercise 5.2 - Create a disabled KMS CMK

Replace `TODO` with your IAM username in `cdk.json` - `iamUsername`.

Execute the following commands to deploy the KMS CMK:
```
npx cdk bootstrap
npx cdk deploy KmsStack
```

The deployed KMS CMK will already be disabled.

## Cleanup

Delete all objects from the S3 Bucket before executing the command!
Execute the following to remove the S3 Bucket and IAM Policy Statements:
```
npx cdk destroy S3Stack
```

Execute the following to remove the KMS CMK:
```
npx cdk destroy KmsStack
```

The CMK will be removed in 7 days because it is the minimum waiting period for a key
scheduled for deletion.
