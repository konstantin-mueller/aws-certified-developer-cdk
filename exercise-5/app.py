#!/usr/bin/env python3
import os

from aws_cdk import core as cdk

from chapter5.kms_stack import KmsStack
from chapter5.s3_stack import S3Stack

app = cdk.App()
environment = cdk.Environment(
    account=os.environ["CDK_DEFAULT_ACCOUNT"], region=os.environ["CDK_DEFAULT_REGION"]
)

S3Stack(app, "S3Stack", env=environment)

KmsStack(app, "KmsStack", env=environment)

app.synth()
