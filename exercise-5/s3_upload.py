import boto3
import json


def main():
    with open("cdk.json", "r") as f:
        bucket_name = json.load(f)["context"]["s3BucketName"]

    resource = boto3.resource("s3")
    bucket = resource.Bucket(bucket_name)

    # PutObject with encryption header
    bucket.put_object(Key="encrypted", Body=b"test", ServerSideEncryption="AES256")
    print(f"Successfully saved the encrypted object 'encrypted' in the S3 Bucket\n\n")

    # PutObject without encryption header -> will raise an AcessDenied error
    bucket.put_object(Key="unencrypted", Body=b"123")


if __name__ == "__main__":
    main()
