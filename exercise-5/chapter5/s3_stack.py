from typing import Any, Mapping
from aws_cdk import aws_iam as iam
from aws_cdk import aws_s3 as s3
from aws_cdk import core as cdk


class S3Stack(cdk.Stack):
    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        """
        Exercise 5.1
        Creates an S3 Bucket and an IAM policy that denies unencrypted uploads.
        """
        super().__init__(scope, construct_id, **kwargs)

        bucket_name = self.node.try_get_context("s3BucketName")
        bucket = s3.Bucket(
            self,
            "s3-bucket",
            bucket_name=bucket_name,
            removal_policy=cdk.RemovalPolicy.DESTROY,
        )

        deny_incorrect_encryption_policy = self.denying_s3_put_object_policy_with(
            sid="DenyIncorrectEncryption",
            bucket_name=bucket_name,
            conditions={
                "StringNotEquals": {"s3:x-amz-server-side-encryption": "AES256"}
            },
        )

        deny_missing_encryption_policy = self.denying_s3_put_object_policy_with(
            sid="DenyMissingEncryption",
            bucket_name=bucket_name,
            conditions={"Null": {"s3:x-amz-server-side-encryption": True}},
        )

        bucket.add_to_resource_policy(deny_incorrect_encryption_policy)
        bucket.add_to_resource_policy(deny_missing_encryption_policy)

    def denying_s3_put_object_policy_with(
        self, sid: str, bucket_name: str, conditions: Mapping[str, Any]
    ) -> iam.PolicyStatement:
        """
        Generates an IAM Policy Statement that denies the S3 put object action on the
        passed S3 Bucket for any principal.

        :param sid: The statement id
        :param bucket_name: The name of the S3 Bucket
        :param conditions: Conditions to add to the statement
        :return: An IAM Policy Statement
        """
        policy = iam.PolicyStatement(
            sid=sid,
            effect=iam.Effect.DENY,
            actions=["s3:PutObject"],
            resources=[f"arn:aws:s3:::{bucket_name}/*"],
            conditions=conditions,
        )
        policy.add_any_principal()
        return policy
