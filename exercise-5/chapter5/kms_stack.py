from aws_cdk import aws_iam as iam
from aws_cdk import aws_kms as kms
from aws_cdk import core as cdk


class KmsStack(cdk.Stack):
    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        """
        Exercise 5.2
        Creates a disabled KMS CMK.
        """
        super().__init__(scope, construct_id, **kwargs)

        key = kms.Key(
            self,
            "KmsCmk",
            enabled=False,
            alias="devassoc-key",
            removal_policy=cdk.RemovalPolicy.DESTROY,
            pending_window=cdk.Duration.days(7),
            description="The CMK for exercise 5.2 of the AWS Certified Developer Study Guide",
        )

        user = iam.User.from_user_name(
            self, "IamUser", user_name=self.node.try_get_context("iamUsername")
        )
        key.grant_admin(user)
        key.grant_encrypt_decrypt(user)
