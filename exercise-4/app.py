#!/usr/bin/env python3
import os

from aws_cdk import core as cdk

from chapter4.rds_stack import RdsStack
from chapter4.dynamodb_stack import DynamodbStack


app = cdk.App()
environment = cdk.Environment(
    account=os.environ["CDK_DEFAULT_ACCOUNT"], region=os.environ["CDK_DEFAULT_REGION"]
)

RdsStack(app, "RdsStack", env=environment)

DynamodbStack(app, "DynamodbStack", env=environment)

app.synth()
