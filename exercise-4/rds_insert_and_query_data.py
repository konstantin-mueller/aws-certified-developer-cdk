"""
Exercises 4.4 and 4.5
Connects to a RDS instance with credentials read from AWS Secretsmanager (secret name is
defined in cdk.json) and endpoint address read from user input.
"""
import json
import uuid
from typing import Tuple

import boto3
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy_guid import GUID


def get_db_credentials() -> Tuple[str, str, str, str]:
    """
    Returns database identifier and name from cdk.json and the username and password
    for the RDS instance from AWS Secretsmanager.  The secret name defined in cdk.json
    is used to read the secret.

    :raises KeyError: If the Secretsmanager response does not contain a secret string or
        if the secret does not contain username and password
    :return: The database identifier, name, username and password of the RDS instance
    """
    with open("cdk.json", "r") as f:
        db_config = json.load(f)["context"]["db"]
    secret_name = db_config["secret_name"]
    db_identifier = db_config["identifier"]
    db_name = db_config["name"]

    client = boto3.client("secretsmanager")
    response = client.get_secret_value(SecretId=secret_name)
    if not "SecretString" in response:
        raise KeyError("No SecretString found in the Secretsmanager response")

    secret = json.loads(response["SecretString"])
    return db_identifier, db_name, secret["username"], secret["password"]


def get_db_endpoint_address(db_identifier: str) -> str:
    client = boto3.client("rds")
    response = client.describe_db_instances(DBInstanceIdentifier=db_identifier)
    return response["DBInstances"][0]["Endpoint"]["Address"]


Base = declarative_base()


class User(Base):
    """
    Entity for the users table
    """

    __tablename__ = "users"
    id = sqlalchemy.Column(GUID(), primary_key=True, default=uuid.uuid4)
    first_name = sqlalchemy.Column(sqlalchemy.String(length=100), nullable=False)
    last_name = sqlalchemy.Column(sqlalchemy.String(length=100), nullable=False)
    email = sqlalchemy.Column(sqlalchemy.String(length=175), nullable=False)

    def __str__(self) -> str:
        return f"User(\n\t{self.id=}\n\t{self.first_name=}\n\t{self.last_name=}\n\t{self.email=}\n)"


def main():
    db_identifier, db_name, db_username, db_password = get_db_credentials()
    db_endpoint = get_db_endpoint_address(db_identifier)
    # Connect to the database
    engine = sqlalchemy.create_engine(
        f"mariadb+mariadbconnector://{db_username}:{db_password}@{db_endpoint}:3306/{db_name}"
    )

    # 4.4 Create table
    Base.metadata.create_all(engine)
    print("Table created!")

    # Create DB session
    Session = sqlalchemy.orm.sessionmaker()
    Session.configure(bind=engine)
    session: sqlalchemy.orm.Session = Session()

    # 4.4 Add users to the table
    new_user1 = User(
        first_name="Casey", last_name="Smith", email="casey.smith@somewhere.com"
    )
    new_user2 = User(
        first_name="Sam", last_name="Smith", email="sam.smith@somewhere.com"
    )
    new_user3 = User(first_name="No", last_name="One", email="no.one@somewhere.com")
    session.add_all([new_user1, new_user2, new_user3])
    session.commit()
    print("Inserted data to database!")

    # 4.5 Query items in the table
    query_result = session.query(User).all()
    result = "\n".join(map(str, query_result))
    print(f"Result of query to the users table:\n{result}")


if __name__ == "__main__":
    main()
