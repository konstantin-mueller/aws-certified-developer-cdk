# AWS Certified Developer Study Guide - Chapter 4 Exercise

This folder contains AWS CDK and Python code to create the infrastructure for the
exercises of Chapter 4. This code has the advantage that the infrastructure can be
destroyed with one command by using the AWS CDK (CloudFormation) instead of manual
cleanup.

## RDS credentials

The SDK code of exercise 4.2 specifies the password directly in the code. This is bad
practice. The password should be saved in AWS Secretsmanager. The code in this folder
creates a secret in the Secretsmanager with the name `rds-credentials` (configurable in
`cdk.json`: `db.secret_name`) and a randomly generated password for the RDS instance.

## Setup

[Python version 3.8 or newer](https://www.python.org/downloads/) must be installed for all exercises.

- Exercises 4.1, 4.2, 4.3 4.6, 4.7 and 4.12:
    - [Node.js 11 or newer](https://nodejs.org/en/)
- Exercises 4.4 and 4.5:
    - [MariaDB Connector/C](https://mariadb.com/products/skysql/docs/clients/mariadb-connectors/connector-c/)

Install the CDK (only necessary for exercises 4.1, 4.2, 4.3, 4.6, 4.7 and 4.12):
```
npm install
```

Create a Python virtual environment and install all requirements (all exercises):
```
python -m venv venv
. venv/bin/activate
python -m pip install -r requirements.txt
```

## Exercises 4.1, 4.2 and 4.3 - Deployment of the RDS database

Change `rds_sg_cidr` in `cdk.json` to your IP address with `/32` at the end to
restrict database access only to you instead of anyone on the internet. Execute the
following command to get your IP address: `curl ifconfig.me`

Execute the following commands for the deployment:
```
npx cdk bootstrap
npx cdk deploy RdsStack
```

This may take a few minutes. Don't abort the command as this can lead to a broken
infrastructure!

## Exercises 4.4 and 4.5 - Create a table and insert and query data

Execute the Python script:
```
python rds_insert_and_query_data.py
```

## Exercise 4.6 - Remove RDS database and security group

No code is necessary for this because the AWS CDK supports removing the infrastructure.
Just execute this:
```
npx cdk destroy RdsStack
```

This may take a few minutes. Don't abort the command as this can lead to a broken
infrastructure!

## Exercise 4.7 - Deployment of the DynamoDB

Execute the following commands:
```
npx cdk bootstrap
npx cdk deploy DynamodbStack
```

## Exercises 4.8 and 4.9 - DynamoDB: Add a user and look up the user

Execute the Python script:
```
python dynamodb_insert_and_query.py
```

## Exercises 4.10 and 4.11 - DynamoDB: Batch writing and scanning

Execute the Python script:
```
python dynamodb_batch_and_scan.py
```

## Exercise 4.12 - Remove the DynamoDB

No code is necessary for this because the AWS CDK supports removing the infrastructure.
Just execute this:
```
npx cdk destroy DynamodbStack
```
