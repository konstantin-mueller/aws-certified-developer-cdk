from typing import Tuple

from aws_cdk import aws_ec2 as ec2
from aws_cdk import aws_rds as rds
from aws_cdk import core as cdk


class RdsStack(cdk.Stack):
    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        vpc, security_group = self.create_vpc_with_security_group()
        self.create_db_instance(vpc, security_group)

    def create_vpc_with_security_group(self) -> Tuple[ec2.Vpc, ec2.SecurityGroup]:
        """
        Exercise 4.1
        Creates a VPC and a security group. A VPC is necessary for a security group in
        CDK.

        :return: The created VPC and security group
        """
        vpc = ec2.Vpc(self, "rds-vpc", cidr="10.0.0.0/24", nat_gateways=0)

        security_group = ec2.SecurityGroup(
            self,
            "rds-security-group",
            vpc=vpc,
            description="RDS Security Group for AWS Dev Study Guide",
            security_group_name="rds-sg-dev-demo",
        )

        security_group.add_ingress_rule(
            ec2.Peer.ipv4(self.node.try_get_context("rds_sg_cidr")),
            ec2.Port.tcp(3306),
        )

        return vpc, security_group

    def create_db_instance(
        self, vpc: ec2.Vpc, security_group: ec2.SecurityGroup
    ) -> None:
        """
        Exercise 4.2 and 4.3
        Creates a MariaDB RDS instance.

        :param vpc: The VPC in which the RDS instance will be deployed
        :param security_group: The security group for the RDS instance
        """
        db_config = self.node.try_get_context("db")

        db_instance = rds.DatabaseInstance(
            self,
            "rds-instance",
            vpc=vpc,
            vpc_subnets=ec2.SubnetSelection(subnet_type=ec2.SubnetType.PUBLIC),
            engine=rds.DatabaseInstanceEngine.maria_db(
                version=rds.MariaDbEngineVersion.VER_10_5_8
            ),
            instance_identifier=db_config["identifier"],
            database_name=db_config["name"],
            instance_type=ec2.InstanceType("t2.micro"),
            credentials=rds.Credentials.from_generated_secret(
                db_config["user_name"], secret_name=db_config["secret_name"]
            ),
            security_groups=[security_group],
            allocated_storage=20,
        )

        db_tags = cdk.Tags.of(db_instance)
        db_tags.add("Purpose", "AWS Developer Study Guide Demo")
