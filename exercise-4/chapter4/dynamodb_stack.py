from aws_cdk import aws_dynamodb as dynamodb
from aws_cdk import core as cdk


class DynamodbStack(cdk.Stack):
    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        """
        Exercise 4.7
        Creates a DynamoDB table.
        """
        super().__init__(scope, construct_id, **kwargs)

        dynamodb.Table(
            self,
            "dynamodb",
            table_name="Users",
            partition_key=dynamodb.Attribute(
                name="user_id", type=dynamodb.AttributeType.STRING
            ),
            sort_key=dynamodb.Attribute(
                name="user_email", type=dynamodb.AttributeType.STRING
            ),
            read_capacity=5,
            write_capacity=5,
            removal_policy=cdk.RemovalPolicy.DESTROY,
        )
