"""
Exercises 4.10 and 4.11
Uses a DynamoDB batch writer to write 100 users into the table and executes a scan.
"""
import json

import boto3

from dynamodb_user import User


def main():
    dynamodb = boto3.resource("dynamodb")
    table = dynamodb.Table("Users")

    with table.batch_writer() as writer:
        for i in range(100):
            writer.put_item(
                Item=User(
                    user_email=f"someone{i}@somewhere.com",
                    user_fname=f"User{i}",
                    user_lname=f"UserLast{i}",
                ).to_dict()
            )
            print(f"Writing record #{i + 1} to DynamoDB Users table")
    print("Saved 100 users in DynamoDB\n")

    scan_result = table.scan()
    print(f"Total count: {scan_result['Count']}")
    print(json.dumps(scan_result["Items"], indent=4))


if __name__ == "__main__":
    main()
