"""
Exercises 4.8 and 4.9
Saves an user in the DynamoDB table and queries and prints it.
"""
import json

import boto3
from boto3.dynamodb import conditions as dynamodb_conditions

from dynamodb_user import User


def main():
    dynamodb = boto3.resource("dynamodb")
    table = dynamodb.Table("Users")

    user = User(
        user_email="someone@somewhere.com", user_fname="Sam", user_lname="Samuels"
    )
    table.put_item(Item=user.to_dict())

    query_result = table.query(
        KeyConditionExpression=dynamodb_conditions.Key("user_id").eq(user.user_id)
    )

    print(json.dumps(query_result["Items"], indent=4))


if __name__ == "__main__":
    main()
