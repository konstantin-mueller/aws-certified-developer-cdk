import dataclasses
import uuid
from typing import Any, Dict, Optional


@dataclasses.dataclass
class User:
    """
    Helper class for a user in the DynamoDB table.
    """

    user_email: str
    user_fname: str
    user_lname: str
    user_id: Optional[str] = None

    def __post_init__(self):
        if not self.user_id:
            self.user_id = str(uuid.uuid4())

    def to_dict(self) -> Dict[str, Any]:
        return dataclasses.asdict(self)
