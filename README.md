# AWS CDK infrastructure code for some of the exercises of the AWS Certified Developer official study guide

---
**Important**

It is expected that the AWS credentials and the default region are configured like
described in exercise 1.3 of the book.

---

This repository is separated into the chapters of the book. Check the READMEs of the
subfolders for further instructions:

- [Chapter 4](exercise-4): RDS instance and DynamoDB
- [Chapter 5](exercise-5): S3 Bucket with IAM Policies that deny unencrypted uploads
and KMS CMK
